using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class InputReader : MonoBehaviour
{
    private List<string> allowedInputs;
    public Material keyboardLitMaterial;
    public Material keyboardUnlitMaterial;
    public KeyCode keyPressed;
    public KeyCode lastKeyPressed;
    public int keysPressed;
    public KeyCode displayKey;
    public RotorBehavior rotorBehavior;
    public Transform keyChild;
    public GameObject[] plugs;
    public bool plugUsed;

    // Start is called before the first frame update
    void Start()
    {
        var tempArray = File.ReadAllLines("Assets/Scripts/AllowedInputs.txt");
        allowedInputs = new List<string>();

        foreach (string input in tempArray)
        {
            allowedInputs.Add(input);
        }

        plugs = GameObject.FindGameObjectsWithTag("Plug");

        keyPressed = KeyCode.None;
        lastKeyPressed = KeyCode.None;
        keysPressed = 0;

        rotorBehavior = GameObject.Find("Rotors").GetComponent<RotorBehavior>();
        plugUsed = false;
    }

    // Update is called once per frame
    void Update()
    {
        KeyBoardChanger();
    }

    public void KeyBoardChanger()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rotorBehavior.MoveRotorUp();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            rotorBehavior.MoveRotorDown();
        }

        foreach (KeyCode vKey in System.Enum.GetValues(typeof (KeyCode)))
        {
            if (allowedInputs.Contains(vKey.ToString()) && Input.GetKey(vKey))
            {
                foreach (GameObject plug in plugs)
                {
                    var plugBehavior = plug.GetComponent<PlugBehavior>();
                    if (plug.name == vKey.ToString() + "Plug" && plugBehavior.isConnected)
                    {
                        keyPressed = (KeyCode)System.Enum.Parse(typeof(KeyCode), plugBehavior.connectedKey);
                        lastKeyPressed = keyPressed;
                        plugUsed = true;
                        break;
                    }
                }
                if (!plugUsed)
                {
                    keyPressed = vKey;
                    lastKeyPressed = keyPressed;
                }
            }
        }

        if (keyPressed != KeyCode.None)
        {
            if (keysPressed == 0)
            {
                displayKey = (KeyCode)System.Enum.Parse(typeof (KeyCode), rotorBehavior.GetOutputKey(keyPressed.ToString()));

                var stringKeyPressed = displayKey.ToString() + " key";
                keyChild = transform.Find(stringKeyPressed);
                    
                keyChild.GetComponent<Renderer>().material = keyboardLitMaterial;
                keysPressed++;
            }
        }

        if (lastKeyPressed != KeyCode.None && lastKeyPressed != keyPressed)
        {
            var stringLastKeyPressed = lastKeyPressed.ToString() + " key";
            
            keyChild.GetComponent<Renderer>().material = keyboardUnlitMaterial;
            keysPressed = 0;
        }

        keyPressed = KeyCode.None;
        plugUsed = false;
    }
}
