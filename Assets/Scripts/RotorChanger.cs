using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotorChanger : MonoBehaviour
{
    public RotorBehavior rotorBehavior;

    // Start is called before the first frame update
    void Start()
    {
        rotorBehavior = GameObject.Find("Rotors").GetComponent<RotorBehavior>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeRotor(int rotorNumber, int index)
    {
        rotorBehavior.SetActiveRotors(index, "Rotor" + rotorNumber);
        rotorBehavior.RedrawActiveRotors();
    }
}
