using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlugManager : MonoBehaviour
{
    public GameObject[] wires;

    // Start is called before the first frame update
    void Start()
    {
        wires = new GameObject[13];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddWire(GameObject wire)
    {
        for (int i = 0; i < wires.Length; i++)
        {
            if (wires[i] == null)
            {
                wires[i] = wire;
                break;
            }
        }
    }

    public void ClearWires()
    {
        for (int i = 0; i < wires.Length; i++)
        {
            if (wires[i] != null)
            {
                var wire = wires[i];
                wires[i] = null;
                Destroy(wire);
            }
        }
    }

}
