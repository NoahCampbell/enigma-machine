using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class RotorBehavior : MonoBehaviour
{
    public GameObject[] activeRotors;
    public Text[,] rotorTexts;
    public bool switchMode;

    // Start is called before the first frame update
    void Start()
    {
        activeRotors = new GameObject[3];
        switchMode = false;

        for (int i = 0; i < activeRotors.Length; i++)
        {
            activeRotors[i] = GameObject.Find("Rotor" + (i + 1));
            activeRotors[i].GetComponent<IndividualRotors>().UpdateRotors(i);
            activeRotors[i].GetComponent<IndividualRotors>().activeRotorIndex = i;
        }

        rotorTexts = new Text[3, 3];  
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DrawActiveRotors()
    {
        activeRotors[0].GetComponent<IndividualRotors>().UpdateRotors(0);
    }

    public void SetActiveRotors(int index, string rotorName)
    {
        activeRotors[index].GetComponent<IndividualRotors>().activeRotorIndex = -1;
        activeRotors[index] = GameObject.Find(rotorName);
        activeRotors[index].GetComponent<IndividualRotors>().activeRotorIndex = index;
    }

    public string GetOutputKey(string inputKey)
    {
        if (switchMode)
        {
            switchMode = false;
        }

        string outputKey = activeRotors[0].GetComponent<IndividualRotors>().GetOutputKey(0, inputKey);

        DrawActiveRotors();

        return outputKey;
    }

    public void RedrawActiveRotors()
    {
        foreach (GameObject rotor in activeRotors)
        {
            rotor.GetComponent<IndividualRotors>().RedrawRotor();
        }
    }

    public void SwitchMode()
    {
        switchMode = true;
    }

    public void MoveRotorDown()
    {
        activeRotors[0].GetComponent<IndividualRotors>().DecrementRotorPosition();
    }

    public void MoveRotorUp()
    {
        activeRotors[0].GetComponent<IndividualRotors>().IncrementRotorPosition();
    }

}
