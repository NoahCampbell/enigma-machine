using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlugBehavior : MonoBehaviour
{
    public GameObject[] plugs;
    public PlugManager plugManager;
    public bool isActive;
    public GameObject connectedPlug;
    public GameObject plugWire;
    public bool isConnected;
    public Material inactivePlugMaterial;
    public Material activePlugMaterial;
    public string connectedKey;

    // Start is called before the first frame update
    void Start()
    {
        plugs = GameObject.FindGameObjectsWithTag("Plug");
        isActive = false;
        connectedPlug = null;
        isConnected = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        if (isActive)
        {
            isActive = false;
            gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().material = inactivePlugMaterial;
        }
        else
        {
            foreach (GameObject plug in plugs)
            {
                if (plug.GetComponent<PlugBehavior>().isActive && !plug.GetComponent<PlugBehavior>().isConnected)
                {
                    ConnectPlugs(plug.transform.position, gameObject.transform.position, plug);
                    return;
                }
            }

            if (connectedPlug == null)
            {
                isActive = true;
                transform.GetChild(0).GetComponent<SpriteRenderer>().material = activePlugMaterial; 
            }
        }
    }

    public void ConnectPlugs(Vector2 startingPosition, Vector2 endingPosition, GameObject plug)
    {
        plug.GetComponent<PlugBehavior>().isActive = false;
        isConnected = true;
        plug.GetComponent<PlugBehavior>().isConnected = true;
        connectedPlug = plug;
        plug.GetComponent<PlugBehavior>().connectedPlug = gameObject;

        connectedPlug.transform.GetChild(0).GetComponent<SpriteRenderer>().material = activePlugMaterial;
        gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().material = activePlugMaterial;
        connectedKey = plug.name.Substring(0, 1);
        connectedPlug.GetComponent<PlugBehavior>().connectedKey = gameObject.name.Substring(0, 1);

        var wire = Instantiate(plugWire);

        plugManager.AddWire(wire);
        wire.transform.position = startingPosition;
        
        var direction = endingPosition - startingPosition;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        wire.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        wire.transform.localScale = new Vector2(Vector2.Distance(startingPosition, endingPosition), plugWire.transform.localScale.y);
    }

    public void ResetPlugBoard()
    {
        foreach (GameObject plug in plugs)
        {
            plug.GetComponent<PlugBehavior>().isActive = false;
            plug.GetComponent<PlugBehavior>().isConnected = false;
            plug.GetComponent<PlugBehavior>().connectedPlug = null;
            plug.transform.GetChild(0).GetComponent<SpriteRenderer>().material = inactivePlugMaterial;
        }

        plugManager.ClearWires();
    }

}
