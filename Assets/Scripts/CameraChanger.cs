using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    public Camera enigmaCamera;
    public Camera plugboardCamera;
    public GameObject[] plugButtons;

    // Start is called before the first frame update
    void Start()
    {
        enigmaCamera = GameObject.Find("Enigma Camera").GetComponent<Camera>();
        plugboardCamera = GameObject.Find("Plug Camera").GetComponent<Camera>();

        plugButtons = GameObject.FindGameObjectsWithTag("Plug");

        for (int i = 0; i < plugButtons.Length; i++)
        {
            plugButtons[i] = plugButtons[i].transform.GetChild(3).gameObject;
            plugButtons[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwitchCameras()
    {
        if (enigmaCamera.enabled)
        {
            enigmaCamera.GetComponent<Camera>().enabled = false;

            plugboardCamera.GetComponent<Camera>().enabled = true;
            foreach (GameObject plug in plugButtons)
            {
                plug.SetActive(true);
            }
        }
        else
        {
            enigmaCamera.GetComponent<Camera>().enabled = true;

            plugboardCamera.GetComponent<Camera>().enabled = false;
            foreach (GameObject plug in plugButtons)
            {
                plug.SetActive(false);
            }
        }
    }

}
