using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;

public class IndividualRotors : MonoBehaviour
{
    public GameObject rotor;
    public string[,] rotorLetters;
    public int rotorPosition;
    public Text rotorTopText;
    public Text rotorMiddleText;
    public Text rotorBottomText;
    public RotorBehavior rotorBehavior;
    public bool recentlyCompleted;
    public int activeRotorIndex;
    public Button incrementButton;
    public Button decrementButton;
    public int rotorNumber;

    // Start is called before the first frame update
    void Start()
    {
        var matches = Regex.Matches(rotor.name, @"\d+");

        rotorNumber = int.Parse(matches[0].Value);

        string[] tempArray = File.ReadAllLines("Assets/Scripts/Rotor" + rotorNumber + "Arrangements.txt");

        rotorLetters = new string[tempArray.Length, 25];


        for (int i = 0; i < tempArray.Length; i++)
        {
            string[] tempArray2 = tempArray[i].Split(':');
            rotorLetters[i, 0] = tempArray2[0];
            
            tempArray2 = tempArray2[1].Split(',');
            for (int j = 1; j < tempArray2.Length; j++)
            {
                rotorLetters[i, j] = tempArray2[j];
            }
        }

        rotorPosition = Random.Range(0, 22);
        rotorBehavior = GameObject.Find("Rotors").GetComponent<RotorBehavior>();

        recentlyCompleted = false;

        activeRotorIndex = -1;

        incrementButton.onClick.AddListener(IncrementRotorPosition);
        decrementButton.onClick.AddListener(DecrementRotorPosition);     
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Shuffles the array of rotor letters (unused)
    // public string[] Shuffle(string[] stringArray)
    // {
    //     string[] tempArray = stringArray;
    //     string[] newArray = new string[tempArray.Length];
    //     int randomIndex;
    //     for (int i = 0; i < tempArray.Length; i++)
    //     {
    //         randomIndex = Random.Range(0, tempArray.Length);

    //         while (tempArray[randomIndex] == null)
    //         {
    //             randomIndex = Random.Range(0, tempArray.Length);
    //         }

    //         newArray[i] = tempArray[randomIndex];
    //         tempArray[randomIndex] = null;
            
    //     }
    //     return newArray;
    // }

    public void UpdateRotors(int rotorIndex)
    {
        
        if (rotorPosition + 2 > 25)
        {
            if (rotorPosition == 25)
            {
                rotorTopText.text = "2";
                rotorMiddleText.text = "1";
                rotorBottomText.text = "25";

                rotorPosition = 1;
            }
            else
            {
                rotorTopText.text = "1";
                rotorMiddleText.text = (rotorPosition + 1).ToString();
                rotorBottomText.text = (rotorPosition).ToString();

                rotorPosition = 0;
                recentlyCompleted = true;
            }
        }
        else if (rotorPosition == 0)
        {
            if (rotorIndex < 2 && recentlyCompleted)
            {
                rotorBehavior.activeRotors[rotorIndex + 1].GetComponent<IndividualRotors>().UpdateRotors(rotorIndex + 1);
            }

            rotorTopText.text = (rotorPosition + 2).ToString();
            rotorMiddleText.text = (rotorPosition + 1).ToString();
            rotorBottomText.text = "25";

            rotorPosition++;
            recentlyCompleted = false;
        }
        else if (rotorPosition == -1)
        {
            rotorPosition = 25;
            rotorTopText.text = "1";
            rotorMiddleText.text = rotorPosition.ToString();
            rotorBottomText.text = (rotorPosition - 1).ToString();
        }
        else 
        {
            rotorTopText.text = (rotorPosition + 2).ToString();
            rotorMiddleText.text = (rotorPosition + 1).ToString();
            rotorBottomText.text = (rotorPosition).ToString();

            rotorPosition++;
        }
    }

    public string GetOutputKey(int rotorIndex, string inputKey)
    {
        string outputKey = "";

        if (recentlyCompleted)
        {
            if (rotorIndex != 2)
            {
                outputKey = rotorBehavior.activeRotors[rotorIndex + 1].GetComponent<IndividualRotors>().GetOutputKey(rotorIndex + 1, inputKey);
            }
            else 
            {
                recentlyCompleted = false;
                outputKey = rotorBehavior.activeRotors[rotorIndex].GetComponent<IndividualRotors>().GetOutputKey(rotorIndex, inputKey);
            }
        }
        else if (rotorPosition == 25)
        {
            for (int i = 0; i < 26; i++)
            {
                if (rotorLetters[i, 0] == inputKey)
                {
                    outputKey = rotorLetters[i - 1, rotorPosition];
                    return outputKey;
                }
            }
        }
        else
        {
            for (int i = 0; i < 26; i++)
            {
                if (rotorLetters[i, 0] == inputKey)
                {
                    outputKey = rotorLetters[i, rotorPosition];
                    return outputKey;
                }
            }
        }
        return outputKey;
    }

    public void RedrawRotor()
    {
        if (activeRotorIndex != -1)
        {
            if (activeRotorIndex == 0)
            {
                rotorBehavior.rotorTexts[0, 0] = rotorTopText;
                rotorBehavior.rotorTexts[0, 1] = rotorMiddleText;
                rotorBehavior.rotorTexts[0, 2] = rotorBottomText;
            }
            else if (activeRotorIndex == 1)
            {
                rotorBehavior.rotorTexts[1, 0] = rotorTopText;
                rotorBehavior.rotorTexts[1, 1] = rotorMiddleText;
                rotorBehavior.rotorTexts[1, 2] = rotorBottomText;
            }
            else if (activeRotorIndex == 2)
            {
                rotorBehavior.rotorTexts[2, 0] = rotorTopText;
                rotorBehavior.rotorTexts[2, 1] = rotorMiddleText;
                rotorBehavior.rotorTexts[2, 2] = rotorBottomText;
            }

            UpdateRotors(activeRotorIndex);
        }
    }

    public void IncrementRotorPosition()
    {
        UpdateRotors(activeRotorIndex);
    }

    public void DecrementRotorPosition()
    {
        rotorPosition -= 2;
        UpdateRotors(activeRotorIndex);
    }
}
